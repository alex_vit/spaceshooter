﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class GameController : MonoBehaviour {
    public GameObject[] stoneObjectPrefabs;
    public Transform[] spawnPoint;
    public PlayerController player;
    public bool isPlaying;
    WaitForSeconds m_SpawnDelay;

    public Action<GameState> OnLevelEnded;
    public Action<GameState> OnStateChanged;

    private static GameController _instance;
    public static GameController Instance {
        get {
            return _instance;
        }
    }

    private List<GameObject> lvlObstacles;
    private LevelData currLevelData;
    private int currLevelNum;

	// Use this for initialization
	void Awake () {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
        m_SpawnDelay = new WaitForSeconds(0.5f);
        lvlObstacles = new List<GameObject>();
        GameData.Instance.Init();
        Debug.Log("game will be continued " + GameData.Instance.gameWasClosed);
    }

    void OnEnable() {
        if (ViewController.Instance)
        {
            ViewController.Instance.OnLevelChosen += Init;
            player.OnStateChanged += ViewController.Instance.UpdateInfo;
            OnLevelEnded += ViewController.Instance.ShowResults;

        }
        player.OnPlayerDied += EndOfGame;
    }

    void OnDisable() {
        if (ViewController.Instance)
        {
            ViewController.Instance.OnLevelChosen -= Init;
            player.OnStateChanged -= ViewController.Instance.UpdateInfo;
            OnLevelEnded -= ViewController.Instance.ShowResults;
        }
        player.OnPlayerDied -= EndOfGame;
    }

    #region game-cycle methods
    public void Init(int levelId)
    {
        currLevelNum = levelId;
        currLevelData = _define.Levels.Where(x => x.levelNum == levelId).First();
        GameData.Instance.SetDefault();
        StartGame();
    }

    public void StartGame()
    {
        player.Activate();
        LoadGameParams();
        if (!isPlaying)
        {
            isPlaying = true;
            Debug.Log(currLevelData.levelNum);
            LoadLevel(currLevelData);
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
    }

    public void ResumeGame() {
        Time.timeScale = 1.0f;
    }

    public void RestartGame()
    {
        GameData.Instance.SetDefault();
        player.Activate();
        player.SetDefault();
        if (!isPlaying)
        {
            isPlaying = true;
            currLevelData = _define.Levels.Where(x => x.levelNum == player.State.levelNum).First();
            LoadLevel(currLevelData);
        }
        StartGame();
    }

    public void StopGame()
    {
        isPlaying = false;
        StopAllCoroutines();
        RemoveAllObstaclesFromScene();
        player.Deactivate();
    }


    #endregion
    public void EndOfGame()
    {
        StopGame();
        
        if (player.State.scores > GameData.Instance.recordsTable[currLevelNum])
        {
            GameData.Instance.UpdateBestScore(currLevelNum, player.State.scores);
            Debug.Log("cur level number " + currLevelNum);
        }
        if (OnLevelEnded != null)
            OnLevelEnded(player.State);
        GameData.Instance.Save();
    }

    public void LevelPassed() {
        Debug.Log("level  passed");
        GameState _state = player.State;
        GameData.Instance.UnlockLevel(Mathf.Clamp(currLevelNum+1, 1, _define.Levels.Length));
        EndOfGame();
    }

    public int GetCurrentLevelNumber() {
        return currLevelNum;
    }

    void LoadGameParams()
    {
        if (GameData.Instance.gameWasClosed)
        {
            GameState _save = new GameState();
            currLevelNum = GameData.Instance.State.levelNum;

            _save.lifes = GameData.Instance.State.lifes;
            _save.scores = GameData.Instance.State.scores;
            _save.levelNum = GameData.Instance.State.levelNum;
            _save.obstaclesCount = GameData.Instance.State.obstaclesCount;
            player.SetState(_save);
        }
        else
        {
            player.SetDefault();
        }
    }

    public void LoadLevel(LevelData level){
        if (!GameData.Instance.gameWasClosed)
            StartCoroutine(SpawnStones(level.stonesCount));
        else
            StartCoroutine(SpawnStones(player.State.obstaclesCount));
    }


    void AddLvlObjectToList(GameObject lvlObject) {
        lvlObstacles.Add(lvlObject);
        lvlObject.GetComponent<ObjectHealth>().OnDeath += RemoveObjectFromList;
    }

    void RemoveObjectFromList(GameObject lvlObject) {
        lvlObject.GetComponent<ObjectHealth>().OnDeath -= RemoveObjectFromList;
        lvlObject.GetComponent<PoolObject>().ReturnToPool();
        Debug.Log("invoked");
        lvlObstacles.Remove(lvlObject);

        GameState _state = player.State;
        _state.obstaclesCount--;
        player.SetState(_state);
        CheckLevelEndCondition();
    }

    void RemoveAllObstaclesFromScene() {
        for (int i = 0; i < lvlObstacles.Count; i++) {
            lvlObstacles[i].GetComponent<ObjectHealth>().OnDeath -= RemoveObjectFromList;
            lvlObstacles[i].GetComponent<PoolObject>().ReturnToPool();
            lvlObstacles.Remove(lvlObstacles[i]);
        }
    }

    void CheckLevelEndCondition() {
        if (isPlaying && lvlObstacles.Count <= 0)
            LevelPassed();
    }

    void OnApplicationQuit() {
        if (isPlaying)
            GameData.Instance.UpdateState(player.State);
        GameData.Instance.Save();
    }

    IEnumerator SpawnStones (int obstaclesCount){
        
        int _currStonesCount = obstaclesCount;
        Debug.Log("_currStonesCount " + _currStonesCount);
        while (isPlaying && _currStonesCount > 0)
        {          
            string _currStoneName = stoneObjectPrefabs[UnityEngine.Random.Range(0, stoneObjectPrefabs.Length)].name;
            Vector3 _currPosition = spawnPoint[UnityEngine.Random.Range(0, spawnPoint.Length)].position;
            GameObject _stone = PoolManager.GetObject(_currStoneName, _currPosition, new Quaternion(0f, 0f, 0f, 0f));
            AddLvlObjectToList(_stone);
            _stone.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _currStonesCount--;
            yield return m_SpawnDelay;
        }
        yield return null;
    }
}
