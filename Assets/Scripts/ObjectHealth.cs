﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public class ObjectHealth : MonoBehaviour {
    public int maxHealth;

    private int currHealth;
    public Action<GameObject> OnDeath;
    public Action OnDmgTaken;
    public bool isAlive;

    public void TakeDamage() {
        currHealth--;
        if (currHealth <= 0 && OnDeath != null)
            OnDeath(gameObject);
        else if (OnDmgTaken != null)
            OnDmgTaken();
    }

    public void DeathImmediately() {
        currHealth = 0;
        if (OnDeath != null)
            OnDeath(gameObject);
    }

    public void SetDefault() {
        currHealth = maxHealth;
    }

    public void SetHealth(int health) {
        currHealth = health;
    }

    public bool IsAlive() {
        return currHealth > 0;
    }
}
