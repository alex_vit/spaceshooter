﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour {
    public Transform player;
    public Vector3[] playerPos;

    private Damager playerDamager;
    private Movable playerMover;
    private ObjectHealth playerHealth;

    public ObjectHealth Health { get { return playerHealth; } }
    public Action OnPlayerDied;

    private GameState gameState;
    public GameState State {
        get { return gameState; } 
    }

    public Action<GameState> OnStateChanged;
	// Use this for initialization
	void Awake () {
        playerDamager = GetComponent<Damager>();
        playerMover = GetComponent<Movable>();
        playerHealth = GetComponent<ObjectHealth>();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            playerDamager.MakeShot();
    }

    void OnEnable() {
        if (playerHealth != null)
            playerHealth.OnDmgTaken += Injure;
        if (playerDamager != null)
            playerDamager.OnEnemyDestroyed += IncreaseScore;
    }

    void OnDisable() {
        if(playerHealth != null)
            playerHealth.OnDmgTaken -= Injure;
        if (playerDamager != null)
            playerDamager.OnEnemyDestroyed -= IncreaseScore;
    }

    public void Activate() {
        if (!gameObject.activeSelf)
            gameObject.SetActive(true);
        playerHealth.SetDefault();
        playerDamager.Activate();
    }

    public void Deactivate() {
        gameObject.SetActive(false);
    }

    public void SetDefault() {
        gameState.levelNum = GameController.Instance.GetCurrentLevelNumber();
        gameState.lifes = playerHealth.maxHealth;
        gameState.scores = 0;
        gameState.obstaclesCount = _define.Levels[GameController.Instance.GetCurrentLevelNumber()-1].stonesCount;
        UpdateInfo();
    }

    public void SetState(GameState state) {
        gameState.levelNum = state.levelNum;
        gameState.lifes = state.lifes;
        gameState.scores = state.scores;
        gameState.obstaclesCount = state.obstaclesCount;
        playerHealth.SetHealth(state.lifes);
        UpdateInfo();
    }

    void Injure() {
        gameState.lifes--;
        UpdateInfo();
        if (!playerHealth.IsAlive())
            Die();
        
    }

    void IncreaseScore() {
        gameState.scores += _define.awardForObstacle;
        UpdateInfo();
    }

    void UpdateInfo() {
        if (OnStateChanged != null)
            OnStateChanged(gameState);
    }

    void Die()
    {
        //some actions when player die, e.g. particles explosion
        if (GameController.Instance.isPlaying && OnPlayerDied != null)
            OnPlayerDied();
    }


}
