﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Stone : MonoBehaviour {
    private ObjectHealth objectHealth;
    private PoolObject objectPool;

	// Use this for initialization
	void Start () {
        objectHealth = GetComponent<ObjectHealth>();
        objectPool = GetComponent<PoolObject>();
	}


    private void Death() {
        Debug.Log("death");
        //create effect and return to pull;
        objectPool.ReturnToPool();
    }

    private void Explose()
    {
        Debug.Log("explose");
        objectPool.ReturnToPool();
    }

    void OnTriggerEnter(Collider coll) {
        if (coll.gameObject.tag.Equals(_define.borderTag))
            objectHealth.DeathImmediately();
        if (coll.gameObject.tag.Equals(_define.playerTag))
        {
            Debug.Log("coll with player");
            coll.gameObject.GetComponent<ObjectHealth>().TakeDamage();
            objectHealth.DeathImmediately();
        }
    }
}
