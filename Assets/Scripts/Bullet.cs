﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bullet : MonoBehaviour {
    PoolObject objectPool;

    public Action<GameObject> OnHit;
    // Use this for initialization
    void Awake() {
        objectPool = GetComponent<PoolObject>();
    }

    void Death() {
        if (OnHit != null)
            OnHit = null;
        objectPool.ReturnToPool();
    }

    void OnCollisionEnter(Collision coll) {
        if (coll.gameObject.CompareTag(_define.stoneTag))
        {
            Debug.Log("coll with stone");
            ObjectHealth _target = coll.gameObject.GetComponent<ObjectHealth>();
            _target.TakeDamage();
            if (!_target.IsAlive() && OnHit != null)
                OnHit(_target.gameObject);
            Death();
        }
    }
}
