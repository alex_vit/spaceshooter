﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Damager : MonoBehaviour {
    public float reloadTime = 0f;
    public Vector3 shotDirection;
    public float shotPower = 0f;
    public GameObject bulletPrefab;
    public Transform[] weaponPlace;
    private WaitForSeconds timeForRecharge;
    private bool isAbleToShot = true;

    public Action OnEnemyDestroyed;
	// Use this for initialization
	void Start () {
        timeForRecharge = new WaitForSeconds(reloadTime);
        isAbleToShot = true;
    }

    public void Activate() {
        isAbleToShot = true;
    }

    public void MakeShot()
    {
        if (GameController.Instance.isPlaying && isAbleToShot)
        {
            CreateShotForEachWeapon();
            isAbleToShot = false;
            StartCoroutine(Recharge());
        }
    }

    private void CreateShotForEachWeapon() {
        for (int i = 0; i < weaponPlace.Length; i++) {
            Rigidbody _bullet = PoolManager.GetObject(bulletPrefab.name, weaponPlace[i].position, new Quaternion(0f, 0f, 0f, 0f)).GetComponent<Rigidbody>();
            _bullet.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _bullet.AddForce(shotDirection * shotPower, ForceMode.Impulse);
            _bullet.GetComponent<Bullet>().OnHit += DestroyTarget;
        }
    }

    private void DestroyTarget(GameObject sender) {
        if (OnEnemyDestroyed != null)
            OnEnemyDestroyed();
    }

    private IEnumerator Recharge()
    {
        yield return timeForRecharge;
        isAbleToShot = true;
    }

    
}
