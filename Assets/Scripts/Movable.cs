﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Movable : MonoBehaviour {
    public float velocityValue;
    public Vector3 rightBorder;
    public Vector3 leftBorder;

    private string axisName = "Horizontal";
    private Rigidbody selfBody;
    // Use this for initialization
    void Awake() {
        selfBody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        if(GameController.Instance.isPlaying)
            Move();
	}

    bool CheckBorders() {
        
        if (transform.position.x <= leftBorder.x)
            transform.position = new Vector3(leftBorder.x, transform.position.y, transform.position.z);
        if(transform.position.x >= rightBorder.x)
            transform.position = new Vector3(rightBorder.x, transform.position.y, transform.position.z);
        return transform.position.x >= leftBorder.x && transform.position.x <= rightBorder.x;
    }

    public void Move() {
        Vector3 _newMovement = transform.right * Input.GetAxis(axisName) * velocityValue * Time.deltaTime;

        if (CheckBorders())
        {
            selfBody.MovePosition(selfBody.position + _newMovement);
            transform.position = new Vector3(transform.position.x + Input.GetAxis(axisName), transform.position.y, transform.position.z);
        }
    }

    public void StopMovement() {
        selfBody.velocity = Vector3.zero;
    }
}
