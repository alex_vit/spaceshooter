﻿using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {
    public int ID;
    public Color openedColor;
    public Color blockedColor;
    public Text bestResultLabel;
    public Text labelText;
        
    private Image selfImage;
    // Use this for initialization
	void Awake () {
        selfImage = GetComponent<Button>().image;
	}

    void OnEnable() {
        Init();
    }

    public void Init() {
        if(selfImage == null)
            selfImage = GetComponent<Button>().image;
        selfImage.color = GameData.Instance.levelInfo[ID] ? openedColor : blockedColor;
        labelText.text = "Level " + ID.ToString();
        if (GameData.Instance.recordsTable[ID] > 0)
        {
            bestResultLabel.text = "Best result : " + GameData.Instance.recordsTable[ID];
            bestResultLabel.gameObject.SetActive(true);
        }

    }
}
