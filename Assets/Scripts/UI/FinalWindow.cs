﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinalWindow : MonoBehaviour {
    public Text resultTextLabel;
    public Text bestResultTextLabel;


    public void Show(GameState state) {
        resultTextLabel.text = _define.scoreMsg + state.scores.ToString();
        bestResultTextLabel.text = _define.bestScoreMsg + GameData.Instance.recordsTable[state.levelNum];
    }
}
