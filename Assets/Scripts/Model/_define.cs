﻿public static class _define{
    public static readonly string stoneTag = "Stone";
    public static readonly string bulletTag = "Bullet";
    public static readonly string borderTag = "Finish";
    public static readonly string playerTag = "Player";

    public static readonly string dataFileName = "saves.dat";

    public static readonly string msgNotOpenedLevel = "This level not opened yet!";

    public static readonly string obstaclesLeftMsg = "OBSTACLES LEFT: ";
    public static readonly string scoreMsg = "YOUR SCORES: ";
    public static readonly string livesMsg = "LIVES: ";
    public static readonly string bestScoreMsg = "BEST RESULT: ";

    public static LevelData[] Levels = {
        new LevelData {levelNum = 1, stonesCount= 20},
        new LevelData {levelNum = 2, stonesCount= 30},
        new LevelData {levelNum = 3, stonesCount= 40},
        new LevelData {levelNum = 4, stonesCount= 50},
        new LevelData {levelNum = 5, stonesCount= 60},
    };

    public static readonly int awardForObstacle = 10;
}

public delegate void GameObjectHandler();

public struct LevelData
{
    public int levelNum;
    public int stonesCount;

}
