﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
public class GameData {
    public int maxOpenedLevel;
    public bool gameWasClosed;
    
    public Dictionary<int, int> recordsTable;
    public Dictionary<int, bool> levelInfo;

    private GameState state;
    public GameState State {
        get { return state; }
    }

    private static GameData instance;
    public static GameData Instance {
        get {
            if (instance == null)
                instance = new GameData();
            return instance;
        }
    }

    public GameData() {
        maxOpenedLevel = 1;

        recordsTable = new Dictionary<int, int>();
        levelInfo = new Dictionary<int, bool>();
        //fill dictionary
        for (int i = 1; i < _define.Levels.Length; i++)
        {
            levelInfo.Add(i, false);
            recordsTable.Add(i, 0);
        }
        levelInfo[1] = true;
    }

    public void Init() {
        instance = LoadDataFromStore();
    }

    public void Load()
    {
        GameData _data = LoadDataFromStore();
        if (_data != null)
        {
            maxOpenedLevel = _data.maxOpenedLevel;
            recordsTable = _data.recordsTable;
            if(gameWasClosed)
                state = _data.state;
        }
    }

    public void Save() {
        SaveDataToStore();
    }

    public void SetDefault() {
        if (gameWasClosed)
            gameWasClosed = false;
    }

    public void UpdateState(GameState gameState) {
        gameWasClosed = true;
        state = gameState;
    }

    public void UpdateBestScore(int levelNum, int newResult) {
        recordsTable[levelNum] = newResult;
    }

    public void UnlockLevel(int levelNum) {
        levelInfo[levelNum] = true;
        maxOpenedLevel = Mathf.Clamp(++maxOpenedLevel, 1, _define.Levels.Length);
    }

    private GameData LoadDataFromStore() {
        GameData _data;
        string _path = Application.persistentDataPath + "/" + _define.dataFileName;
        try
        {
            FileStream _fs = new FileStream(_path, FileMode.Open);
            BinaryFormatter _bf = new BinaryFormatter();
            _data = (GameData)_bf.Deserialize(_fs);
            _fs.Close();
            return _data;
        }
        catch (Exception e) {
            Debug.Log(e.Message);
            return null;
        }
    }

    private void SaveDataToStore() {
        BinaryFormatter _bf = new BinaryFormatter();
        string _path = Application.persistentDataPath + "/" + _define.dataFileName;
        FileStream _fs = new FileStream(_path, FileMode.OpenOrCreate);
        _bf.Serialize(_fs, this);
        _fs.Flush();
        _fs.Close();
    }
}

[Serializable]
public struct GameState
{
    public int scores;
    public int lifes;
    public int levelNum;
    public int obstaclesCount;
}
