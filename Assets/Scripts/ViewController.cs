﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System;


public class ViewController : MonoBehaviour {
    public GameObject MenuPanel;
    public GameObject MapPanel;
    public GameObject GamePanel;
    private GameObject m_ActivePanel;

    public float msgTextDuration;
    public Text scoreTextLabel;
    public Text infoTextLabel;
    public Text msgTextLabel;
    public Text obstaclesCountLabel;

    public LevelButton[] levelButtons;

    public FinalWindow finalPopup;
    public GameObject gameBackButton;

    public Action<int> OnLevelChosen;

    private static ViewController _instance;
    public static ViewController Instance
    {
        get
        {
            return _instance;
        }
    }

    private WaitForSeconds waitForMsgText;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else
            Destroy(gameObject);
        waitForMsgText = new WaitForSeconds(msgTextDuration);
    }

    void Start() {
        ActivatePanel(MenuPanel);
        Init();
    }

    void OnEnable() {
        Init();
    }

    void Init()
    {
        for (int i = 0; i < levelButtons.Length; i++) {
            levelButtons[i].Init();
        }
    }

    public void ActivatePanel(GameObject panel)
    {
        if (m_ActivePanel != null)
            m_ActivePanel.SetActive(false);
        m_ActivePanel = panel;
        m_ActivePanel.SetActive(true);
    }

    public void CheckLastGame()
    {
        if (GameData.Instance.gameWasClosed)
        {
            ActivatePanel(GamePanel);
            GameController.Instance.StartGame();
        }
        else
            ActivatePanel(MapPanel);
    }

    public void UpdateInfo(GameState state) {
        scoreTextLabel.text = _define.scoreMsg + state.scores.ToString();
        infoTextLabel.text = _define.livesMsg + state.lifes.ToString();
        obstaclesCountLabel.text = _define.obstaclesLeftMsg + state.obstaclesCount.ToString();
    }

    public void ShowResults(GameState state) {
        gameBackButton.SetActive(false);
        finalPopup.gameObject.SetActive(true);
        finalPopup.Show(state);
    }

    public void ShowMessage(string msg) {
        if (!msgTextLabel.gameObject.activeSelf)
        {
            msgTextLabel.text = msg;
            msgTextLabel.gameObject.SetActive(true);
            StartCoroutine(ShowMsg());
        }
    }

    public void OnLevelButtonClick(int ID) {
        //check if available
        if (GameData.Instance.levelInfo[ID] && OnLevelChosen != null)
        {
            OnLevelChosen(ID);
            ActivatePanel(GamePanel);
            gameBackButton.SetActive(true);
        }
        else
            ShowMessage(_define.msgNotOpenedLevel);
    }


    IEnumerator ShowMsg() {
        yield return waitForMsgText;
        msgTextLabel.gameObject.SetActive(false);
    }
}
