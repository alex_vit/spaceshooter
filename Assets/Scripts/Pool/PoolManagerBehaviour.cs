﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Pool/PoolManagerBehaviour")]
public class PoolManagerBehaviour : MonoBehaviour {
        #region Unity scene settings
        [SerializeField] private PoolManager.PoolInfo[] pools;
        #endregion

        #region Methods
        void OnValidate()
        {
            for (int i = 0; i < pools.Length; i++)
            {
                pools[i].name = pools[i].prefab.name;
            }
        }

        void Awake()
        {
            Initialize();
        }

        void Initialize()
        {
            PoolManager.Initialize(pools);
        }
        #endregion
}
